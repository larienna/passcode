demo: obj/passcode.o obj/main.o
	gcc -o demo obj/passcode.o obj/main.o

obj/passcode.o: src/lib/passcode.c
	gcc -c -o obj/passcode.o src/lib/passcode.c -Isrc/lib/

obj/main.o: src/ex/main.c
	gcc -c -o obj/main.o src/ex/main.c -Isrc/lib/

test: obj/unitest.o obj/unitest00.o obj/unitest01.o obj/passcode.o
	gcc -o test obj/unitest.o obj/unitest00.o obj/unitest01.o obj/passcode.o -lcunit

obj/unitest.o: src/test/unitest.c
	gcc -c -o obj/unitest.o src/test/unitest.c -Isrc/lib -Isrc/test

obj/unitest00.o: src/test/unitest00.c
	gcc -c -o obj/unitest00.o src/test/unitest00.c -Isrc/lib -Isrc/test

obj/unitest01.o: src/test/unitest01.c
	gcc -c -o obj/unitest01.o src/test/unitest01.c -Isrc/lib -Isrc/test

.PHONY: clean

clean:
	rm -f obj/*.o

