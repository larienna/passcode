/*
   PASS CODE

   Example program to demonstrate the code.

   @author: Eric Pietrocupo
   @date: December 11th, 2018
*/


#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>
#include <passcode.h>

int main()
{  unsigned long xormask = 0xAAAAAAAAAA;
   int prime = 31;
   char *base32 = "ABCDEFGHIJKLMNOPQRSTUWXYZ34789@%";
   unsigned char data [CROSSWORD_DATA_BYTE_SIZE] =
      { 0x12, 0x34, 0x56, 0x78, 0x9A, 0xBC, 0xDE, 0xF0 };
   unsigned char answer [CROSSWORD_DATA_BYTE_SIZE];
   char password [17];

   //validate the parameters and return the error code
   int errorcode = validate_crossword_cipher( xormask, prime, base32);

   //if the cipher is not valid, return the error code
   if ( errorcode != PC_PARAM_VALID)
   {  printf ("\nERROR: Cipher parameters invalid");
      return errorcode;
   }

   //create a cipher with the same parameter.
   s_crossword_cipher *cipher = create_crossword_cipher( xormask, prime, base32);

   if ( cipher != NULL )
   {  printf ("Source data is: ");
      print_byte_array( data, CROSSWORD_DATA_BYTE_SIZE );

      //encode the data with the cipher and return the value in the password parameter.
      encode_with_crossword( cipher, data, password );
      printf ("\n\nThe Password is : %s", password );

      //Decode the password, extract the data and put it into the answer parameter
      decode_with_crossword( cipher, answer, password );
      printf ("\n\nDecoded data is: ");
      print_byte_array( answer, CROSSWORD_DATA_BYTE_SIZE );
   }
   else printf ("\nERROR: Creating cipher");

   //the cipher is dynamically allocated, so free must be called
   free(cipher);

   printf ("\n");
   return 0;
}



