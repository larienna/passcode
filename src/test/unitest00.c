/**
   PASSCODE
   Unit test suite 00 : contains general use purpose methods

   @author: Eric Pietrocupo
   @date: May 10th, 2021
*/

#include <stdbool.h>
#include <passcode.h>
#include <passcode_test.h>
#include <CUnit/CUnit.h>

int TS00_setup ( void )
{  return 0;
}

int TS00_teardown ( void )
{  return 0;
}

void TS00_compare_bits_diff ( void )
{
   unsigned char testbyte_A = 0b11001100;
   unsigned char testbyte_B = 0b11100110;
   unsigned char testbyte_array_A [4] = { 0xFA, 0x85, 0xB3, 0xD9 };
   unsigned char testbyte_array_B [4] = { 0xEA, 0x86, 0xA4, 0xD9 };

   CU_ASSERT_EQUAL ( compare_bits_diff ( &testbyte_A, &testbyte_B, 1), 3);
   CU_ASSERT_EQUAL ( compare_bits_diff ( &testbyte_array_A[0], &testbyte_array_B[0], 4), 7);

}

void TS00_byte_array_conversion ( void )
{  unsigned long longint = 0xAABB116699DD4422;
   unsigned char bytearray [CROSSWORD_DATA_BYTE_SIZE]= { 0xAA, 0xBB, 0x11, 0x66, 0x99, 0xDD, 0x44, 0x22};
   unsigned char bytearray_answer[CROSSWORD_DATA_BYTE_SIZE] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

   CU_ASSERT_EQUAL ( _byte_array_to_long ( bytearray), longint);
   _long_to_byte_array ( bytearray_answer, longint );
   CU_ASSERT_NSTRING_EQUAL ( bytearray_answer, bytearray, CROSSWORD_DATA_BYTE_SIZE );

}

void TS00_base32segment_conversion ( void )
{  unsigned long b32longint = 0x000000BB116699DD;
   unsigned char bytearray [CROSSWORD_SAVE_BYTE_SIZE]= { 0x00, 0xBB, 0x11, 0x66, 0x99, 0xDD, 0x00, 0x00, 0x00, 0x00};
   unsigned char bytearray_answer[CROSSWORD_SAVE_BYTE_SIZE] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

   CU_ASSERT_EQUAL ( _byte_array_to_long_base32_segment (bytearray, 1), b32longint);
   _long_to_byte_array_base32_segment( bytearray_answer, b32longint, 1);
   CU_ASSERT_NSTRING_EQUAL( bytearray_answer, bytearray, CROSSWORD_SAVE_BYTE_SIZE);
}

void TS00_unique_char ( void )
{  char *uniquestr = "abcdefghijklmnopqrstuvwxyz";
   char *nonuniquestr = "the quick brown fox jumps over the lazy dog";

   CU_ASSERT_TRUE ( _unique_char_in_str( uniquestr ) );
   CU_ASSERT_FALSE ( _unique_char_in_str( nonuniquestr ) );
}

void TS00_validate_prime_number ( void )
{  CU_ASSERT_TRUE ( _valid_prime_number (2) );
   CU_ASSERT_TRUE ( _valid_prime_number (3) );
   CU_ASSERT_TRUE ( _valid_prime_number (5) );
   CU_ASSERT_TRUE ( _valid_prime_number (7) );
   CU_ASSERT_TRUE ( _valid_prime_number (13) );
   CU_ASSERT_TRUE ( _valid_prime_number (19) );
   CU_ASSERT_TRUE ( _valid_prime_number (31) );
   CU_ASSERT_TRUE ( _valid_prime_number (43) );
   CU_ASSERT_TRUE ( _valid_prime_number (59) );
   CU_ASSERT_TRUE ( _valid_prime_number (71) );
   CU_ASSERT_FALSE ( _valid_prime_number (4) );
   CU_ASSERT_FALSE ( _valid_prime_number (9) );
   CU_ASSERT_FALSE ( _valid_prime_number (25) );
   CU_ASSERT_FALSE ( _valid_prime_number (49) );
}

void TS00_even_bits ( void )
{  CU_ASSERT_FALSE ( _even_one_and_zero(0b1011001001100111000111100110110111001011) );
   CU_ASSERT_TRUE ( _even_one_and_zero(0b0011001011000110010111000110001111001011) );
}

void TS00_xoring ( void )
{  unsigned char data [4] = {0b10110010, 0b10110110, 0b00011001, 0b11001001};
   unsigned long mask = 0b0000000000000000000000000000000010010100011011100111001100111011;
   unsigned char answer [4] = {0b00100110, 0b11011000, 0b01101010, 0b11110010};

   _xor_savedata( data, mask, 0, 4 );
   CU_ASSERT_NSTRING_EQUAL( data, answer, 4);
}

void TS00_bitarray_conversion ( void )
{  unsigned char source[2] = {0b10011001, 0b01010101};
   unsigned char destination[2];
   unsigned char array[16];
   unsigned char answer[16] = {1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1 };

   _bits_to_array( source, array, 2);
   CU_ASSERT_NSTRING_EQUAL( array, answer, 16 );
   _array_to_bits( destination, array, 2 );
   CU_ASSERT_NSTRING_EQUAL( destination, source, 2);

}

void TS00_prime_dispersion (void)
{
   unsigned char source[2] = { 0b10011001, 0b01010101 };
   unsigned char answer[2] = { 0b01110111, 0b00010001 };

   _apply_prime_dispersion ( source, 7, 2 );
   CU_ASSERT_NSTRING_EQUAL( source, answer, 2 );
}

void TS00_prime_reordering (void)
{
   unsigned char source[CROSSWORD_SAVE_BYTE_SIZE] = { 18, 52, 86, 120, 154, 188, 222, 240, 68, 0};
   unsigned char copy[CROSSWORD_SAVE_BYTE_SIZE] = { 18, 52, 86, 120, 154, 188, 222, 240, 68, 0};

   _apply_prime_dispersion ( copy, 31, CROSSWORD_SAVE_BYTE_SIZE );
   _apply_prime_dispersion ( copy, 31, CROSSWORD_SAVE_BYTE_SIZE );
   CU_ASSERT_NSTRING_EQUAL( source, copy, CROSSWORD_SAVE_BYTE_SIZE );
}


void TS00_encode_base32 (void)
{
   unsigned char source[CROSSWORD_SAVE_BYTE_SIZE] = { 0x69, 0x8E, 0xA9, 0x91, 0x2C, 0x69, 0x8E, 0xA9, 0x91, 0x2C};
   char *symbols = "QWERTYUIOPASDFGHJKLZXCBNM34789^*";
   char password [17];
   char *answer = "FUIAZTPDFUIAZTPD";
   _encode_base32 ( source, password, symbols );
   CU_ASSERT_EQUAL ( strcmp (password, answer), 0 );
}

void TS00_decode_base32 (void)
{
   unsigned char answer[CROSSWORD_SAVE_BYTE_SIZE] = { 0x69, 0x8E, 0xA9, 0x91, 0x2C, 0x69, 0x8E, 0xA9, 0x91, 0x2C};
   unsigned char destination[CROSSWORD_SAVE_BYTE_SIZE] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
   char *symbols = "QWERTYUIOPASDFGHJKLZXCBNM34789^*";
   char password [17] = "FUIAZTPDFUIAZTPD";

   _decode_base32 ( destination, password, symbols );
   CU_ASSERT_NSTRING_EQUAL ( answer, destination, CROSSWORD_SAVE_BYTE_SIZE );
}

void TS00_register_suite ( void )
{  CU_pSuite TS00 = CU_add_suite("00 General functions", TS00_setup, TS00_teardown);

   if (TS00 != NULL)
   {  CU_add_test (TS00, "Compare bits difference", TS00_compare_bits_diff );
      CU_add_test (TS00, "Byte to array conversion", TS00_byte_array_conversion );
      CU_add_test (TS00, "Base32 segment Byte to array conversion", TS00_base32segment_conversion );
      CU_add_test (TS00, "Unique char in string", TS00_unique_char );
      CU_add_test (TS00, "Validate prime numbers", TS00_validate_prime_number );
      CU_add_test (TS00, "Verify mask bits", TS00_even_bits );
      CU_add_test (TS00, "Xoring", TS00_xoring );
      CU_add_test (TS00, "Bit array conversion", TS00_bitarray_conversion );
      CU_add_test (TS00, "Prime dispersion", TS00_prime_dispersion );
      CU_add_test (TS00, "Prime reordering", TS00_prime_reordering );
      CU_add_test (TS00, "Encode base 32", TS00_encode_base32 );
      CU_add_test (TS00, "Decode base 32", TS00_decode_base32 );
   }
}


