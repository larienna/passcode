/**
   test.c

   Random test  that were used during the creation of the software.
   Copied here for preservation.

   @author: Eric Pietrocupo
   @date: May 23rd, 2021
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>
#include <passcode.h>
#include <unitest.h>

/*
// verifying probability to forge a valid code with only parity check
*/

/*void test_parity ()
{  //unsigned char datavalue [CROSSWORD_DATA_BYTE_SIZE]; // originally random generated value
   unsigned char savedata [CROSSWORD_SAVE_BYTE_SIZE]; // encrypted data including parity
   int i;
   int good = 0;
   int bad = 0;
   time_t t;
   srand((unsigned) time(&t));



   for ( i = 0 ; i < 10000000; i++)
   {
      //savedata[HOR_PARITY_BYTE] = rand() * 255;
      //savedata[VER_PARITY_BYTE] = rand() * 255;

      //print_byte_array( savedata, CROSSWORD_SAVE_BYTE_SIZE);

      generate_data ( savedata, CROSSWORD_SAVE_BYTE_SIZE );

      if ( verify_parity (savedata) == true)
      {  good++;
      }
      else
      {  bad++;
      }
      //print_byte_array(savedata, CROSSWORD_DATA_BYTE_SIZE);
   }

   printf ("\nGood parity = %d", good);
   printf ("\nBad parity = %d", bad );
}*/

/*
   basic code generation and verification. Does the whole process once.
*/

void test_basic ()
{  unsigned char datavalue [CROSSWORD_DATA_BYTE_SIZE]; // originally random generated value
   unsigned char savedata [CROSSWORD_SAVE_BYTE_SIZE]; // encrypted data including parity
   //unsigned char hackedata [CROSSWORD_SAVE_BYTE_SIZE]; // modified copy
   time_t t;
   srand((unsigned) time(&t));

   generate_data ( datavalue, CROSSWORD_DATA_BYTE_SIZE );

   printf ("\nGenerated Data");
   print_byte_array( datavalue, CROSSWORD_DATA_BYTE_SIZE);

   memcpy (savedata, datavalue, CROSSWORD_DATA_BYTE_SIZE );
   //memcpy (hackedata, datavalue, CROSSWORD_SAVE_BYTE_SIZE );
   //set_parity( savedata );
   printf ("\nAfter Parity");
   print_byte_array( savedata, CROSSWORD_SAVE_BYTE_SIZE);

   //set_xoring( savedata );

   printf ("\nAfter Xoring");
   print_byte_array( savedata, CROSSWORD_SAVE_BYTE_SIZE);
/*
   //hackedata[HOR_PARITY_BYTE] = (hackedata[HOR_PARITY_BYTE] ^ 1);
   //hackedata[VER_PARITY_BYTE] = (hackedata[VER_PARITY_BYTE] ^ 1);
   hackedata[5] = (hackedata[5] ^ 1 ); //change 1 bit
   //hackedata[5]++;
   set_parity (hackedata);
   set_xoring (hackedata);

   printf ("\nHacked Data, Parity, xoring");
   print_byte_array( hackedata, CROSSWORD_SAVE_BYTE_SIZE);

   printf ("\nNumber of bits which are different = %d",
      compare_bits_diff( savedata, hackedata, CROSSWORD_SAVE_BYTE_SIZE));*/

   /*if ( verify_parity (hackedata) == true)
   {  printf ("\nParity is valid");
   }
   else
   {  printf ("\nParity is not valid");
   }*/



   //unsigned long savevalue = byte_array_to_long( savedata );
   //unsigned long hackvalue = byte_array_to_long( hackedata );

   //printf ("\nSave   Data: %0lX", savevalue);
   //printf ("\nHacked Data: %0lX", hackvalue);

//   set_dispersion( savedata, 31 );
   printf ("\nThe dispersed array is");
   print_byte_array (savedata, CROSSWORD_SAVE_BYTE_SIZE);

   char code [17];
//   encode_base32( savedata, code, "");
   printf ("\nThe password is : %s", code );

   //unsigned long dispersedvalue = byte_array_to_long( savedata );
   //printf ("\nDispersed Data: %0lX", dispersedvalue);


   /*   unsigned char array[8] = {0,0,0,0,0,0,0,0};
   printf ("Converting 0F0E0D0C0B0A0908\n");
   long_to_byte_array ( array, 0x0F0E0D0C0B0A0908 );
   printf ("Results\n");
   print_byte_array( array, CROSSWORD_DATA_BYTE_SIZE );
   printf ("\nReconverting back the array");
   unsigned long value = byte_array_to_long( array );
   printf ("\nResult: %0lX", value);*/

}

void test_algoprime ()
{  char array[80];
   int i, j;
   int index = 0;

   for ( i = 0 ; i < 80; i++) array[i] = 0;

   for ( i = 0 ; i < 80 ; i++)
   {  index += 31; //nb premier: 7,11,13,(17),19,23,29,(31),37,41,43,47,53,59,61,(67),71,73,79
      index = index % 80;
      //array[index]++; //test collision
      array[index] = i / 8; // can test for strength of dispersion
      // touching bytes of same value, or of value +/- 1, odd/even adjacence?(not sure)
      // distance between bytes
      printf ("\n%02d:", i);
      for ( j = 0 ; j < 80 ; j++) printf ("%d", array[j]);
   }


}

void test_xoring ()
{
   unsigned char datavalue [CROSSWORD_DATA_BYTE_SIZE] = { 0, 0, 0, 0, 0, 0, 0, 0  }; // originally random generated value
   unsigned char savedata [CROSSWORD_SAVE_BYTE_SIZE]; // encrypted data including parity
   unsigned char hackedata [CROSSWORD_SAVE_BYTE_SIZE]; // modified copy
   int i;
   int changes[80];
   //int byte_changed[8] = {0,0,0,0,0,0,0,0};
   //int bit_changed[8] = {0,0,0,0,0,0,0,0};
   int diff;
   time_t t;
   srand((unsigned) time(&t));

   // initialise count
   for ( i = 0 ; i < 80 ;i++) changes [i] = 0;

   for ( i = 0 ; i < 1280000; i++)
   {
      generate_data ( datavalue, CROSSWORD_DATA_BYTE_SIZE );

      memcpy (savedata, datavalue, CROSSWORD_DATA_BYTE_SIZE );
      memcpy (hackedata, datavalue, CROSSWORD_SAVE_BYTE_SIZE );
      //set_parity( savedata );
      //set_xoring( savedata );
      hackedata[i%8] = (hackedata[i%8] ^ (int) pow (2, ((i % 64)/8))); //change 1 bit
      //hackedata[5] = (hackedata[5] ^ 1 ); //change 1 bit
      //set_parity (hackedata);
     // set_xoring (hackedata);

      diff = compare_bits_diff( savedata, hackedata, CROSSWORD_SAVE_BYTE_SIZE);
      //changes[diff]++;
      //printf ("\n%d : %d", i%8, (int) pow (2, ((i % 64)/8)));
      //printf ("\n%d", pow(2, );
      /*if ( diff == 11 )
      {
         printf ("Byte %d, bit %d\n", i%8, ((i % 64)/8) );
         print_byte_array( datavalue, CROSSWORD_DATA_BYTE_SIZE );
         print_byte_array( savedata, CROSSWORD_SAVE_BYTE_SIZE);
         print_byte_array( hackedata, CROSSWORD_SAVE_BYTE_SIZE);

         //changes[i%64]++;
         //byte_changed[i%8]++;
         //bit_changed[((i % 64)/8)]++;

      }*/
      changes[diff]++;



   }



   printf ("\nBit differences results");

   for ( i = 0 ; i < 80 ; i++)
   {  if ( changes[i] > 0 ) printf ("\n%2d bits: %6d = %d%%", i, changes[i], changes[i] * 100 / 1280000 );
   }

   /*printf ("\nByte / bit affected");
   for ( i = 0 ; i < 64 ; i++)
   {  if ( changes[i] > 0 ) printf ("\n%2d / %2d : %6d ", i%8, ((i % 64)/8), changes[i] );
   }*/

   /*printf ("\nByte affected");
   for ( i = 0 ; i < 8 ; i++) printf ("\n%d : %d", i, byte_changed[i]);

   printf ("\nBit affected");
   for ( i = 0 ; i < 8 ; i++) printf ("\n%d : %d", i, bit_changed[i]);*/

}


#define ARRAY_SIZE 16
#define PRIME 7
void debug_algorith ( void )
{
   unsigned char src [ARRAY_SIZE] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 };
   unsigned char dst [ARRAY_SIZE];
   unsigned char dst2 [ARRAY_SIZE];

   for( int i = 0 ; i < ARRAY_SIZE ; i++ )
   {  dst[i] = 0;
      dst2[i] = 0;
   }

   print_byte_array( src, ARRAY_SIZE );
   int index = PRIME - 1;
   for ( int i = 0 ; i < ARRAY_SIZE ; i++)
   {
      index = index % ARRAY_SIZE;
      dst[i] = src[index];
      index += PRIME;
   }

   print_byte_array( dst, ARRAY_SIZE );

   index = PRIME -1;
   for ( int i = 0 ; i < ARRAY_SIZE ; i++)
   {
      index = index % ARRAY_SIZE;
      dst2[i] = dst[index];
      index += PRIME;
   }

   print_byte_array( dst2, ARRAY_SIZE );

}

