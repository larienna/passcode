/**
   PASSCODE-TEST

   @author: Eric Pietrocupo
   @date: May 16th, 2021

   This file contains a declaration of private methods to make the unit test have access
   to the methods.

*/

#ifndef PASSCODE_TEST_H_INCLUDED
#define PASSCODE_TEST_H_INCLUDED

#ifdef __cplusplus
extern "C"
{
#endif

unsigned long _byte_array_to_long ( unsigned char *data);
void _long_to_byte_array ( unsigned char *data, unsigned long value);
unsigned long _byte_array_to_long_base32_segment ( unsigned char *data, int first);
void _long_to_byte_array_base32_segment ( unsigned char *data, long value, int first);

unsigned char _horizontal_parity ( unsigned char *save );
unsigned char _vertical_parity ( unsigned char *save );

bool _unique_char_in_str ( char *str );
bool _valid_prime_number ( int number );
bool _even_one_and_zero ( unsigned long mask );
void _xor_savedata (unsigned char *save, unsigned long xormask, int index, int size);
void _bits_to_array ( unsigned char *save, unsigned char *array, int size );
void _array_to_bits ( unsigned char *save, unsigned char *array, int size );
void _apply_prime_dispersion ( unsigned char *save, int prime, int size );
//void _apply_prime_reordering ( unsigned char *save, int prime );
void _encode_base32 ( unsigned char *save, char *password, char *symbol );
void _decode_base32 ( unsigned char *save, char *password, char *symbol );

#ifdef __cplusplus
}
#endif

#endif // PASSCODE_TEST_H_INCLUDED
