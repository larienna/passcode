/**
   PASSCODE
   Unit test suite 00 : contains general use purpose methods

   @author: Eric Pietrocupo
   @date: May 10th, 2021
*/

#include <stdbool.h>
#include <CUnit/CUnit.h>
#include <time.h>
#include <stdlib.h>
#include <passcode.h>
#include <passcode_test.h>



int TS01_setup ( void )
{  return 0;
}

int TS01_teardown ( void )
{  return 0;
}

void TS01_parity_check ( void)
{  unsigned char bytearray [CROSSWORD_DATA_BYTE_SIZE] = { 0b10101010,
                                           0b10010001,
                                           0b00100010,
                                           0b00000100,
                                           0b01010101,
                                           0b01110110,
                                           0b10111011,
                                           0b11011111,
                                          };
   unsigned char horparity = 0b01010101;
   unsigned char verparity = 0b01011010;

   CU_ASSERT_EQUAL( _horizontal_parity (bytearray), horparity );
   CU_ASSERT_EQUAL( _vertical_parity (bytearray), verparity );
}

void TS01_encode_decode (void)
{  unsigned char src [CROSSWORD_DATA_BYTE_SIZE];
   unsigned char dst [CROSSWORD_DATA_BYTE_SIZE];
   char password [17];

   //Setting up the randomiser
   time_t t;
   srand((unsigned) time(&t));

   //initializing the data with random value
   generate_data( src, CROSSWORD_DATA_BYTE_SIZE);

   //make sure the destination array is zeroed
   for ( int i = 0 ; i < CROSSWORD_DATA_BYTE_SIZE; i++ ) dst[i] = 0;

   //creating a cipher using a simple mask, 31 as prime number and a series of base32 characters
   s_crossword_cipher *cipher = create_crossword_cipher( 0xAAAAAAAAAA, 31, "ABCDEFGHIJKLMNOPQRSTUWXYZ34789@%");

   //make sure the returned cipher is not null
   CU_ASSERT_NOT_EQUAL( cipher, NULL);

   //encode the source data with the cipher
   CU_ASSERT_TRUE ( encode_with_crossword( cipher, src, password ) );

   //decode recover the data using the password into the destination array
   CU_ASSERT_TRUE (decode_with_crossword( cipher, dst, password ));

   //Make sure the source data match the recovered data
   CU_ASSERT_NSTRING_EQUAL( src, dst, CROSSWORD_DATA_BYTE_SIZE);

   free(cipher);

}


void TS01_register_suite ( void )
{  CU_pSuite TS01 = CU_add_suite("01 Crossword MAC", TS01_setup, TS01_teardown);

   if (TS01 != NULL)
   {  CU_add_test (TS01, "Parity check", TS01_parity_check );
      CU_add_test (TS01, "Encoding and decoding", TS01_encode_decode );
   }
}


