/**
   PASSCODE
   Unit test registry

   @author: Eric Pietrocupo
   @date: May 10th, 2021
*/

#include <unitest.h>
#include <stdbool.h>
#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>

void main ( void )
{  run_all_test();
}

int run_all_test ( void )
{
   if ( CU_initialize_registry() == CUE_SUCCESS)
   {  TS00_register_suite ();
      TS01_register_suite ();

      CU_basic_set_mode(CU_BRM_VERBOSE);
      CU_basic_run_tests();
   }

   CU_cleanup_registry();
   return CU_get_error();
}
