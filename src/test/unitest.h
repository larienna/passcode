/**
   PASSCODE
   Unit test

   @author: Eric Pietrocupo
   @date: May 10th, 2021

   This is a series of test suite registerng routines that will register each test individually.

   Make sure that the registry is initiallised correctly first. The run_all_test() method should
   take care of that and lauch all test suite registering functions.

*/

void TS00_register_suite ( void );
void TS01_register_suite ( void );
int run_all_test ( void );

