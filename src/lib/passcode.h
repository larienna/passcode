/**
   PASS CODE

   @author: Eric Pietrocupo
   @date: December 11th, 2018

   This is a simple cryptographic algorith that allow creating passwords like used in NES
   video games to save data. It has been made only for experimental purposes.

   WARNING: THIS ALGORITHM IS NOT A MATHEMATICALLY PROVEN ALGORITHM AND IT SHOULD NOT BE
      USED FOR CRYPTOGRAPHY. THIS ALGORITHM USE BASIC CRYPTOGRAPHY AND SCRAMBLING, I EXPECT
      THAT T WILL BE CRACKED. DO NOT ENCODE ANY IMPORTANT INFORMATION.

   The basic idea is to supply a series of parameters to the algorithm with data to encrypt.
   It will return a base32 string of characters as a password. Then this password can
   be used in the decrypting function using the same parameters to recover the original data.

   Look at the README.md file for more information about how the ciphers works.
*/

#ifndef PASSCODE_H_INCLUDED
#define PASSCODE_H_INCLUDED

#ifdef __cplusplus
extern "C"
{
#endif

/* The first things to use a the structure that will contain all the parameters
   necessary to create the password. It's important that exactly the same parameters
   are used to create the password and decode the password. Else it will not work. The
   parameters are contained into this hidden structure to ensure the integrity of the content.

   There is a structure for each cipher since the required parameters are not the same.

   NOTE: The CYCLONE cipher is not yet available.
*/

typedef struct s_crossword_cipher s_crossword_cipher;

/* The methods used to validate and initialise the structure above is the following. The validation
   function will return an error code indicating which the parameters is invalid. The creation
   function will create the cipher an return NULL in case of error without any indication why.
*/

int validate_crossword_cipher ( unsigned long xor_mask, int prime_number, char *base32_string);

/* The error codes are the following
*/

#define PC_PARAM_VALID                    0 //
#define PC_PARAM_NON_EXISTING_MAC         1 //the MAC type does not exist
#define PC_PARAM_MAC_NOT_IMPLEMENTED      2 //the MAC type has not yet been implemented
#define PC_PARAM_BAD_XORMASK_NB_BIT       3 //the mask does not have the same number of 0 and 1
#define PC_PARAM_NOT_PRIME                4 //the prime number is not a prime number
#define PC_PARAM_PRIME_DIVIDE_SIZE        5 //the prime number divides the number of bit of the code
#define PC_PARAM_NON_UNIQUE_BASE32_STRING 6 //there are some duplicate characters in the string
#define PC_PARAM_INVALID_SIZE             7 //nb of character not allowed for this MAC
#define PC_PARAM_PRIME_LOWEREQUAL_ZERO    8 //the prime number <= 0
#define PC_PARAM_PRIME_GREATER_THAN_SIZE  9 //the prime number is greater than the number of bits.

/*
   All cipher will require a string of 32 characters for base32 conversion. Preferably,
   you should use characters that are resistant to hand writting deformation and similarity.
   For example: 6 and G, I and 1, 8 and &, T and +, etc.

   The XOR mask is contained in a long integer but only the 40 right most bits are used and
   validated. The xor mask must always have an even number of 1 and 0. The objective is to invert
   half of the bits to avoid weak passwords if all the data is made of zeros. Which is likely to
   happen for example at the start of a video game when nothing has been achieved yet.

   CROSSWORD will require a prime number that does not divide 80 bits, a string of 32 unique
   character for base32 conversion and a xoring mask.

   Now one things to remember is that the combination of those parameters constitute in a certain
   way your key to encode and decode message. So if you do not want non-authorised parties to
   decrypt your passwords, you must keep those parameters secret.

   Once your parameter has been validated to can instantiate your cipher. The cipher is
   dynamically allocated and must be freed before the program closes.
*/

s_crossword_cipher *create_crossword_cipher ( unsigned long xor_mask, int prime_number, char *base32_string);


/** The following constant hold the size of the data that you can encode with CROSSWORD.
   Useful for declaring your array of data. Internally, 2 bytes are added for parity.
*/

#define CROSSWORD_DATA_BYTE_SIZE 8
#define CROSSWORD_SAVE_BYTE_SIZE 10


/* Then you can use the various encoding and decoding method to create password and decode them.
   The content of the array passed in parameter will be modified according to the operation.
   Encoding will modify the password string while decoding will modify the data array.
   On failure, the function below return false.

   CROSSWORD requires an array of 8 bytes as data while the password must be an array of 16+1
   character to hold the password string. You must make sure that arrays of the right size
   are supplied, else you'll get a segmentation fault.

*/

bool encode_with_crossword ( s_crossword_cipher *cipher, unsigned char *data, char *password);
bool decode_with_crossword ( s_crossword_cipher *cipher, unsigned char *data, char *password);




/* ----- Complementary functions -----

   The methods below are used for various purposes. They have no use in the encryption process
   itself.

   The following method is used for testing results. It compares the number of bits that are different
   between each byte array. Usefull to compare 2 generated password when the source data changed.
*/

int compare_bits_diff ( unsigned char *data1, unsigned char *data2, int size);

/* This method creates a byte array of random data used to create data arrays. It's mainly used for
   testing purpose. Don't forget to seed the randomisation algorithm with:

   time_t t;
   srand((unsigned) time(&t));
*/

void generate_data ( unsigned char *data, int size );

/* This is a debugging method that prints an array of byte on the screen.
*/

void print_byte_array ( unsigned char *data, int size );


#ifdef __cplusplus
}
#endif



#endif // PASSCODE_H_INCLUDED
