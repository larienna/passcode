/**
   PASSCODE

   @author: Eric Pietrocupo
   @date: December 11th, 2018
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include <string.h>
#include <passcode.h>

#define BYTE_MASK 0b11111111  // extract 1 byte at a time by ANDing the mask
#define BASE32_MASK  0b00011111
#define CROSSWORD_BIT_SIZE 80 //size in bit of the crossword password
#define MASK_SIZE 40          //number of bits of the xor mask
//#define INVALID_STRING "invalid" //password returned if the cipher parameters are invalid.
#define BIT_MASK 0b00000001  // starting value to check parity.
#define HOR_PARITY_BYTE 8
#define VER_PARITY_BYTE 9

struct s_crossword_cipher
{  unsigned long xor_mask; //mask used to xor the messge. The number of 1 and 0 must be the same.
                           //only the right most 40 bits are used.
   int           prime; //a prime number that does not divide the code bit size
   char          base32[32]; //list of unique characters used for the base 32 conversion
};

//NOTE: The cyclone cipher is not yet available, this is just a plan.
struct s_cyclone_cipher
{  unsigned long xor_mask; //mask used to xor the messge. The number of 1 and 0 must be the same.
                           //only the right most 40 bits are used.
   int           prime; //a prime number that does not divide the code bit size
   char          base32[32]; //list of unique characters used for the base 32 conversion
   int           size; //number of characters of the password. It's relative to the mac_type
   unsigned int  divider; //number used to do the binary CRC division
};


/*-----------------------------------------------------------------------------*/
/*-                           Private Methods                                 -*/
/*-----------------------------------------------------------------------------*/

/*
   Convert an array of 8 bytes into a single long integer
*/
unsigned long _byte_array_to_long ( unsigned char *data)
{  unsigned long increment = 0;
   unsigned long value = 0;
   int i;

   for ( i = 0 ; i < 8 ; i++)
   {  value = value << 8;
      increment = data [i];
      value += increment;
   }

   return value;
}

/*
   Convert an unsigned long integer into an array of bytes.
   The array must have been previously allocated.
*/
void _long_to_byte_array ( unsigned char *data, unsigned long value)
{  int i;
   unsigned long extract = 0;

   for ( i = 7 ; i >= 0 ; i--)
   {  extract = (value & BYTE_MASK);
      data[i] = extract;
      value = value >> 8;
   }
}


/*
   Extract 5 byte from an array of byte into a long for easy base32 processing. The first
   byte where to start the extraction must be specified.
*/
unsigned long _byte_array_to_long_base32_segment ( unsigned char *data, int first)
{  unsigned long increment = 0;
   unsigned long value = 0;
   int i;

   for ( i = first ; i < (first + 5) ; i++)
   {  value = value << 8;
      increment = data [i];
      value += increment;
   }

   return value;
}

void _long_to_byte_array_base32_segment ( unsigned char *data, long value, int first)
{  unsigned long extract = 0;

   for ( int i = 4 ; i >= 0 ; i--)
   {  extract = (value & BYTE_MASK);
      data[i+first] = extract;
      value = value >> 8;
   }
}

/*
   Check the horizontal parity and return the parity that should be applied
   Can be used for applying or verifying the parity.
*/

unsigned char _horizontal_parity ( unsigned char *save )
{  unsigned char mask;
   bool odd;
   unsigned char parity = 0;
   unsigned char bitvalue = 128; // start with the leftmost bit

   for ( int i = 0; i < CROSSWORD_DATA_BYTE_SIZE; i++)
   {  mask = BIT_MASK;
      odd = false;
      for ( int j = 0 ; j < 8 ; j++)
      {  // building up the parity byte
         if ( (save[i] & mask) > 0) // there is a bit in that position
         {  odd = odd == false ? true : false; //invert boolean
         }
         mask = mask << 1;
      }

      if ( odd == true)
      {  parity += bitvalue; // set the bit
      }

      bitvalue /= 2;
   }

   return parity;
}

/*
   Check the vertical parity and return the required parity.
*/
unsigned char _vertical_parity ( unsigned char *save )
{  unsigned char parity = 0;

   for ( int i = 0; i < CROSSWORD_DATA_BYTE_SIZE; i++)
   {  parity = ( parity ^ save[i]);
   }

   return parity;
}

bool _unique_char_in_str ( char *str )
{  int length = strlen ( str );

   for ( int i = 0; i < length; i++ )
   {  for ( int j = 0 ; j < length; j++)
      {  if ( i != j && str[i] == str [j] )
         {  return false;
         }
      }
   }
   return true;
}

/** A simple agorithm to detect prime numbers, but the algorithm is slow because it test all
    possibilities until a divider is found, so it should be used for small numbers only.
*/
bool _valid_prime_number ( int number )
{
   for ( int i = 2; i < number ;i++)
   {  if ( number % i == 0 ) return false;
   }

   return true;
}

/** Verify that the bits in the mask have en even number of bits. Even if a long is used,
only the first right 40 bits are used.
*/
bool _even_one_and_zero ( unsigned long mask )
{  int count = 0;
   unsigned long tested_bit = BIT_MASK;

   for (int i = 0; i < MASK_SIZE ; i++)
   {  if ( (mask & tested_bit) > 0) count++;
      tested_bit = tested_bit<<1;
   }

   return ( count == (MASK_SIZE/2));
}

/** This method will apply a xor mask of [size*8] bits on [size] consecutive bytes starting from
    the [index] position. You must make sure to supply the right values to avoid overflow.
    due to the size of an unsigned long, the size cannot exceed 8 bytes, else you'll just be
    xoring with zeroes.
*/
void _xor_savedata (unsigned char *save, unsigned long xormask, int index, int size)
{  unsigned char bytemask;
   for ( int i = index + size-1; i >= index ; i--)
   {  bytemask = ( BYTE_MASK & xormask);
      xormask = xormask>>8;
      save[i] = ( save[i] ^ bytemask );
   }
}


/** put individual bits into an array to make the reordering of bits by the prime algorithm
    easier to accomplish. You must supply the save data, the destination array of the appropriate
    size (size*8)and the size of the data in bytes. array will be rewriten
*/

void _bits_to_array ( unsigned char *save, unsigned char *array, int size )
{  const int bitsize = size * 8;
   int bitmask; // mask used to isolate each bit

   for ( int i = 0 ; i < bitsize; i++) array [i] = 0;

   for ( int i = 0 ; i < size; i++)
   {  bitmask = BIT_MASK;
      for ( int j = 7 ; j >= 0; j--)
      {  if ( (save[i] & bitmask) > 0) array[(i*8)+j] = 1;
         bitmask = bitmask << 1;
      }
   }
}

/** Reverse process where you use an array to reposition individual bits. save will be rewritten.
*/

void _array_to_bits ( unsigned char *save, unsigned char *array, int size )
{  int value; // decimal value of a bit
   for ( int i = 0 ; i < size; i++) save[i] = 0;

   //conversion from bits to data
   for ( int i = 0 ; i < size; i++)
   {  value = 1;
      for ( int j = 7 ; j >= 0; j--)
      {  if (array [(i*8)+j] == 1) save[i] += value;
         //printf ("\nvalue %d", value);
         value = value * 2;
      }
   }
}



/*
   Disperse the bits according to a pattern. Uses a prime number, some number are better
   than others. The array of data passed in parameter is modified with the dispersion.
   This method should be safe for both encoding and decoding. The size is in byte.
   Running the algorith twice will undo the dispersion and get back to the original data
*/
void _apply_prime_dispersion ( unsigned char *save, int prime, int size )
{  int index;
   const int bitsize = size * 8;
   // easier to manipulate than individual bits
   unsigned char *src_array = malloc (sizeof ( unsigned char) * bitsize );
   unsigned char *dst_array = malloc (sizeof ( unsigned char) * bitsize );;

   _bits_to_array( save, src_array, size);

   // dispersion algoritm
   index = prime -1;
   for ( int i = 0 ; i < bitsize ; i++)
   {  index = index % bitsize;
      dst_array[i] = src_array[index];
      index += prime;
   }

   _array_to_bits( save, dst_array, size);
   free (src_array);
   free (dst_array);
}



/*
   Converts an array of 10 bytes as a base 32 string of 16 characters. The string passed in
   parameter will contain the answer and must be 17 chars (16 char + \0). The list of symbols
   is passed as a string of 33 characters (32 char + \0)
*/
void _encode_base32 ( unsigned char *save, char *password, char *symbol )
{  unsigned long segment;
   unsigned long extract;
   int i;

   //Processing the left 5 bytes
   segment = _byte_array_to_long_base32_segment( save, 0);
   for ( i = 7; i >= 0; i--)
   {  extract = (segment & BASE32_MASK);
      password[i] = symbol [extract];
      segment = segment >> 5;
   }
   //printf ("\nThe segment is: %d", segment);

   //Processing the right 5 bytes
   segment = _byte_array_to_long_base32_segment( save, 5);
   for ( i = 15; i >= 8; i--)
   {  extract = (segment & BASE32_MASK);
      password[i] = symbol [extract];
      segment = segment >> 5;
   }
   //printf ("The segment is: %d", segment);
   password [16] = '\0';

}

/** Decode the string in base 32. The save parameter will be modified
*/
void _decode_base32 ( unsigned char *save, char *password, char *symbol )
{  unsigned char ascii [128]; //map of ascii char symbol to value. Avoid searching each character
   unsigned long segment; //hold temporary decoded data

   //Copy symbol value in the ascii table.
   for ( unsigned char i = 0 ; i < 32; i++ )
   {  ascii [(unsigned)symbol[i]] = i;
   }

   //Processing the 8 first characters
   segment = 0;
   for ( int i = 0 ; i < 8; i++)
   {  segment = segment << 5;
      segment = segment + ascii [(unsigned)password[i]];
   }
   _long_to_byte_array_base32_segment ( save, segment, 0);

   //Processing the 8 last characters
   segment = 0;
   for ( int i = 8 ; i < 16; i++)
   {  segment = segment << 5;
      segment = segment + ascii [(unsigned)password[i]];
   }
   _long_to_byte_array_base32_segment ( save, segment, 5);

}


/*-----------------------------------------------------------------------------*/
/*-                         Public Methods                                    -*/
/*-----------------------------------------------------------------------------*/

int validate_crossword_cipher ( unsigned long xor_mask,
                               int prime_number, char *base32_string)
{  if ( !_unique_char_in_str( base32_string) ) return PC_PARAM_NON_UNIQUE_BASE32_STRING;
   if ( prime_number <= 0 ) return PC_PARAM_PRIME_LOWEREQUAL_ZERO;
   if ( CROSSWORD_BIT_SIZE % prime_number == 0 ) return PC_PARAM_PRIME_DIVIDE_SIZE;
   if ( prime_number >= CROSSWORD_BIT_SIZE ) return PC_PARAM_PRIME_GREATER_THAN_SIZE;
   if ( !_valid_prime_number( prime_number ) ) return PC_PARAM_NOT_PRIME;
   if ( !_even_one_and_zero ( xor_mask ) ) return PC_PARAM_BAD_XORMASK_NB_BIT;

   return PC_PARAM_VALID;
}

s_crossword_cipher *create_crossword_cipher ( unsigned long xor_mask, int prime_number,
                                             char *base32_string)
{  if ( validate_crossword_cipher( xor_mask, prime_number, base32_string ) == PC_PARAM_VALID)
   {  s_crossword_cipher *cipher = malloc ( sizeof (s_crossword_cipher));
      strncpy ( cipher->base32, base32_string, 32);
      cipher->prime = prime_number;
      cipher->xor_mask = xor_mask;
      return cipher;
   }
   return NULL;
}

bool encode_with_crossword ( s_crossword_cipher *cipher, unsigned char *data, char *password)
{  unsigned char savedata [CROSSWORD_SAVE_BYTE_SIZE]; // encrypted data including parity
   if ( cipher == NULL )
   {  return false;
   }

   memcpy (savedata, data, CROSSWORD_DATA_BYTE_SIZE );
   savedata[HOR_PARITY_BYTE] = _horizontal_parity( data );
   savedata[VER_PARITY_BYTE] = _vertical_parity (data);
   _xor_savedata( savedata, cipher->xor_mask, 0, 5 );
   _xor_savedata( savedata, cipher->xor_mask, 5, 5 );
   _apply_prime_dispersion( savedata, cipher->prime, CROSSWORD_SAVE_BYTE_SIZE );
   _encode_base32( savedata, password, cipher->base32);

   return true;
}

bool decode_with_crossword ( s_crossword_cipher *cipher, unsigned char *data, char *password)
{  unsigned char savedata [CROSSWORD_SAVE_BYTE_SIZE]; // encrypted data including parity
   if ( cipher == NULL ) return false;

   _decode_base32( savedata, password, cipher->base32);
   _apply_prime_dispersion( savedata, cipher->prime, CROSSWORD_SAVE_BYTE_SIZE );
   _xor_savedata( savedata, cipher->xor_mask, 0, 5 );
   _xor_savedata( savedata, cipher->xor_mask, 5, 5 );
   if ( savedata[HOR_PARITY_BYTE] != _horizontal_parity( savedata )) return false;
   if ( savedata[VER_PARITY_BYTE] != _vertical_parity( savedata )) return false;
   memcpy ( data, savedata, CROSSWORD_DATA_BYTE_SIZE );

   return true;
}



int compare_bits_diff ( unsigned char *data1, unsigned char *data2, int size)
{  int nb_bits = 0;
   int i, j;
   int mask;

   for ( i = 0 ; i < size; i++) // byte loop
   {  mask = BIT_MASK;
      for ( j = 0 ; j < 8 ; j++) // bit loop
      {  if ( (data1[i] & mask) != (data2[i] & mask)) nb_bits++;
         mask = mask << 1;
      }
   }

   return nb_bits;
}

void generate_data ( unsigned char *data, int size )
{  int i;

   for ( i = 0 ; i < size ; i++ )
   {  data[i] = rand() % 255;
   }
}


/*
   Print an array of individual bytes on the screen for debuggin purpose.
*/
void print_byte_array ( unsigned char *data, int size )
{  int i;
   printf ("\n");
   for ( i = 0 ; i < size ; i++)
   {  printf ("[%d]", data[i]);
   }
   //printf ("\n");
}
