# Pass Code

Author: Eric Pietrocupo

This is a cryptographic library that can be used to convert an array of data into a base 32 string of text in order to create NES style video game passwords. Then the password can be used to recover the data. An message authentication code is included to make it harder to forge passwords.

**WARNING: This is not a cryptophically secure library. It does not uses any mathematically proven algorithm. DO NOT encrypt any important information with theses algorithms.**

This project was created for the fun of attempting a cipher, so please enjoy hacking the codes if you want. The key space of this cipher is estimated to a bit more than 32! keys which is not enough to ensure security. The key is the combination of all the parameters used by the cipher. If you don want the password to be created by third parties, you must keep that selection of parameters secret.

There will be multiple cipher available as I explore new algoriths. Right now, one cipher is available and another one is in design. The naming convention is the name of the cipher + a number representing the number of characters generated.

**Crossword16** : This algorith use 2D parity to create validation code.

**Cyclone8,16,24,32** : __UNDER CONSTRUCTION__ This algorithm is more flexible and uses an 8 bit CRC as validation code.

## How it works

The basic concept is that you create a cipher using a series of parameters. Then you encode and decode data with the cipher. The same combination of parameters must be used for both the encoding and decoding to make sure it works.

### Crossword cipher: Encoding

Details
: Data length: 64 bit
: Message autentication code length: 16 bit
: Message autentication code type: 2D parity
: Total lenght: 80 bit
: Password character length: 16 characters

Requirements
: 64 bits of data
: 40 bits XOR mask with an even number of ones and zeroes
: A prime number between 3 and 79 that does not divide 80
: A string of 32 unique characters

#### 1. Message Authentication Code: two dimension parity 

The goal of the parity is to create a Message Autentication Code (MAC). It make sure that this configuration of data requires the following parity code for the whole array to be valid. If a MAC was not present, then any sequence of characters would be considered valid password and could be used as data.

The first step consist in doing an horizontal parity by taking each byte of data, and adding a 1 or 0 at the end to complete the parity. This parity is stored into the 9th byte. Then the same must be done with vertical parity, every byte is XORed with each other to obtain the vertical parity that is stored into the 10th byte. Here is an example of 2D parity:

~~~
01100101 | 0   Horizontal parity
00010110 | 1       01010101
01100110 | 0
10001010 | 1
11001100 | 0
10111100 | 1
10110111 | 0
00010000 | 1
__________
01011100       Vertical Parity
~~~

2D parity is weak to single bit changes. If only 1 bit of data change, only 3 bits in total will change which could make it easy to identify them. A recommended solution is to reserve a portion of the data as a counter you increment each time you generate a new password. If you reserve 4 bits to your counter, that means the values will cycle between 0 and 15. This will make sure that more than 3 bits will change.

#### 2. XORing with a mask

The second step consist in using a mask and perform a bit by bit XOR operation on both the data and the parity. The goal is to prevent having a situation where all the bit are set to 0. At the beginning of a game, when little progress have been made, it is quite possible that all data will consist of zeroes. Making parity also worth zeroes, and generating a password made of the same character. The goal of the xor mask is to invert half the bit in the data to make sure there is a good mount of ones for the steps to come.

The xoring is made in 2 portions, the mask is applied once on the first 5 bytes, and another time on the 5 last bytes:

~~~
| Byte 0-4 | Byte 5-9 |
| Xor Mask | Xor Mask |
~~~

The xoring process consist simply of doing a bit by bit XOR operation between the data and the mask. Here is an example of xor on a simple byte:

~~~
Original Data: 10011101
Mask:          01010101
Modified Data: 11001100
~~~

#### 3. Bit dispersion

This is a special algorithm where the goal is to change the position of the bit in the array of data in order to scramble the information. The algorithm requires a prime number between 3 and 79 that does not divide 80. The process consist in adding the prime number to the array index before each iteration, and copying the value into the destination array. If the index exceed the size of the data, a modulo is applied to make it loop back. At the end of 80 iterations, every bit would have been copied once.

Here is an illustration of the process on a single byte. The number represents the Index number of the bit to show you how they are repositioned.

~~~
Prime Number	5
Original byte	01234567

1st pass        4.......
2nd pass        41......
3rd pass        416.....
...
8th Pass        41630527
~~~

#### 4. Base 32 encoding

To make the data readable by humans, the data is converted into a string of base 32 characters. You should try to select characters which are resistant to hand writting if they are to be noted down. The folowing selection of characters are relatively resistant to hand writting: `ABCDEFGHIJKLMNOPQRSTUWXYZ34789@%`. This sequence of character is the most important portion of your encoding key, so make sure you change the order of the characters and keep it secret.

The process is very simple, for each 5 bits, a character from the array is selected. Here is an example using only 20 bits, therefore 4 characters:

~~~
Array of character: ABCDEFGHIJKLMNOPQRSTUWXYZ34789@%

Bits of data:       01100-10011-01001-00101
Password letter:      M     T     J     F
~~~

### Crossword cipher: Decoding

The decoding process consist of the same step but in reverse order. Most of the step consist in applying the same algorithm again to get back the original data.

#### 1. Base 32 decoding

Find the matching letter, and get the integer value associated to it.

#### 2. Bit Dispersion

Apply the bit dispersion algorithm again to get back the original data.

#### 3. Xoring with a mask

XOR the data with the same mask to recover the original data.

#### 4. Verify Parity

Calculate the parity on the 64s bit of data and verify if both parity value match the parity in the code. If not, then the code is erronous. If yes, then the code is valid and the data should be considered.

## How to build

### Demo and test programs

There is currently a simple make file that is going to either build a demo or a cunit tester program. To build the demo, simply type:

`make`

at the root directory, then type

`./demo`

to run the demo. The demo basically takes an array of data, generate the password, then use that password to recover the data. The source file could be used as
a good usage example. It's located at `src/ex/main.c`. Else most of the documnentation is located in the header file itself `src/lib/passcode.h`.

### Making your own program

There is presently no library builder. You need to add those 2 files to your project:

`src/lib/passcode.h`: contains all the information on how to use the library and the header file.

`src/lib/passcode.c`: The source code that must be used by your project.








 
